from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

# Create your views here.
def login_view(request):
    error = 'Please Enter A Correct Username and or Password'
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('home:landing')
    else:
        form = AuthenticationForm()
    response = {
        'form': form,
        'error': error
    }
    return render(request, 'accounts/login.html', response)

def logout_view(request):
    user = request.user
    if user.is_authenticated:
        logout(request)
        return redirect('home:landing')
    return redirect('accounts:login')

def signUp(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home:landing')
    else:
        form = UserCreationForm()
    response = {
        'form': form
    }
    return render(request, 'accounts/signup.html', response)