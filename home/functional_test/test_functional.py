from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
import time

class TestProjectMyStatus(StaticLiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)
        user = User.objects.create_user('Admin', '', 'admin')

    def tearDown(self):
        self.browser.close()

    def test_ada_title_website(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url)

        # User melihat title website yang dibuka tertulis My Status
        self.assertIn('Welcome', self.browser.title)
    
    def test_bisa_login(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url + '/accounts/login')

        userField = self.browser.find_element_by_name('username')
        passField = self.browser.find_element_by_name('password')

        userField.send_keys('Admin')
        passField.send_keys('admin')
        
        button = self.browser.find_element_by_id('login-button')
        button.click()
        
        self.assertIn('Admin', self.browser.page_source)

    def test_bisa_logout(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url + '/accounts/login')

        userField = self.browser.find_element_by_name('username')
        passField = self.browser.find_element_by_name('password')

        userField.send_keys('Admin')
        passField.send_keys('admin')
        
        button = self.browser.find_element_by_id('login-button')
        button.click()
        time.sleep(5)

        buttonOut = self.browser.find_element_by_id('logout-button')
        buttonOut.click()
        time.sleep(5)
        
        self.assertNotIn('Admin', self.browser.page_source)