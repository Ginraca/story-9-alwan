from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse

# Create your tests here.
class UnitTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_ada_url_home(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
    
    def test_ada_template_untuk_landing_page(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'home/landing.html')

    def test_ada_url_untuk_login(self):
        response = self.client.get(reverse('accounts:login'))
        self.assertEqual(response.status_code, 200)

    def test_url_login_ada_template(self):
        response = self.client.get(reverse('accounts:login'))
        self.assertTemplateUsed(response, 'accounts/login.html')